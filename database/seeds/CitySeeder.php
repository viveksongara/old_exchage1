<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
           'city_name' => 'Katni',
           'state_id'  => '1'
        ]);
        
        DB::table('cities')->insert([
           'city_name' => 'Jabalpur',
           'state_id'  => '1'
        ]);
        
        DB::table('cities')->insert([
           'city_name' => 'Allahabad',
           'state_id'  => '2'
        ]);
        
        DB::table('cities')->insert([
           'city_name' => 'Lucknow',
           'state_id'  => '2'
        ]);
    }
}
