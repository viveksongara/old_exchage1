<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        
        DB::table('orders')->insert([
           'product_id' => '1',
           'user_id'    => '2',
           'order_status' => '1',
           'order_price'        => '100',
           'created_at'   => '2016-09-18 11:11:11'
        ]);
        
        DB::table('orders')->insert([
           'product_id' => '2',
           'user_id'    => '2',
           'order_status' => '1',
           'order_price'        => '100',
           'created_at'   => '2016-09-19 11:11:11'
        ]);
        
        DB::table('orders')->insert([
           'product_id' => '3',
           'user_id'    => '2',
           'order_status' => '1',
           'order_price'        => '100',
           'created_at'   => '2016-09-20 11:11:11'
        ]);
    }
}
