@extends('layouts/admin/master')
@section('mainContent')

<div >
   

</div>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     
        <div class="row">
       
        <div class="col-sm-12">
            <div class="panel panel-default">
                
                <div class="panel-heading">
                    <h3 class="panel-title">Update/Edit category</h3>
                </div>
                <div class="panel-body">
                     @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                   
                    @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
                    <form method='post' enctype="multipart/form-data" action="{{url('update-cat')}}">
                        {{csrf_field()}}
                        
                        
                            <div class="form-group">
                                <label>Old category name</label>
                                <select name="old_cat_id" class="form-control">
                                    
                                    <option value="">Choose</option>
                                    @foreach($categories as $category)
                                    <option @if(old('old_category_id') == $category['category_id']) selected @endif value="{{$category['category_id']}}">{{$category['category_name']}}</option>
                                    @endforeach

                                </select>

                            </div>

                       
                        <div class="form-group">
                            <label for="input"> New Category Name </label>
                            <input name="cat_name" value="{{old('cat_name')}}" type="text" class="form-control" id="cat_name" placeholder="enetr new category name">
                        </div>
                         
                        <input class="btn btn-success" type="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>    
    </div> 

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection

