<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password');
            $table->integer('role_id')->default(1);
            $table->integer('activation_status');
            $table->integer('country_id');
            $table->integer('state_id');
            $table->integer('city_id');
            
            $table->string('profile_picture');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('company');
            $table->string('confirmation_code');
            $table->integer('postcode');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
