
   @extends('layouts/frontend/master')
   @section('content')
 


    

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     
        <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"Read categories</h3>
                </div>
                <div class="panel-body">
                     @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Product name</th>
                                <th>Product description</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                                @foreach($products as $product)
                                <tr>
                                    <td>{{$product['product_title']}}</td>
                                    
                                    <td>{{$product['product_description']}}</td>
                                    <td> <img class="primary-image"  style="width:80px"src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}" alt="" /></td>
                                </tr>
                                @endforeach
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>    
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @endsection



