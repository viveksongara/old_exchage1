@extends('layouts/frontend/master')
@section('content')	
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12 mt20">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Post a product</h3>
                        </div>
                        <div class="panel-body">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            
                            @if(Session::has('message'))
                            <div class='alert alert-success'>
                                {{Session::get('message')}}
                            </div>
                            @endif
                            <form method='post' enctype="multipart/form-data" action="{{url('user/make-product-posted')}}">
                                {{csrf_field()}}
                                <div class="form-group">

                                    <select id="categories" name="category_id" class="form-control">
                                        <option value="">Choose Category</option>
                                        @foreach($categories as $category)
                                        <option @if(old('category_id') == $category['category_id']) selected @endif value="{{$category['category_id']}}">{{$category['category_name']}}</option>
                                        @endforeach

                                    </select>

                                </div>
                                
                                
                                <div class="form-group">

                                    <select id="subcategories" name="sub_category_id" class="form-control">
                                        <option value="sub_category_id">Choose sub Category</option>
                                        

                                    </select>

                                </div>
                                
                                <!-- Product Type -->
                                
                                <div class="form-group">
                                    <label for="input">Product Tittle</label>
                                    <input name="product_title" value="{{old('product_title')}}" type="text" class="form-control" id="product_title" placeholder="Product Tittle">
                                </div>
                                 <div class="form-group">

                                    <select name="product_type" class="form-control">
                                        <option value="">Item Looking or Offering</option>
                                        
                                        <option @if(old('product_type') == 'offering') 
                                        selected @endif value="offering">Item offering</option>
                                        <option @if(old('product_type') == 'looking') 
                                        selected @endif value="looking">Item Looking</option>
                                        

                                    </select>

                                </div>   
 
                                 <div class="form-group">
                                    <label for="input">Item offering</label>
                                    <input name="offering" value="{{old('offering')}}" type="text" class="form-control" id="offering" placeholder="Item offering">
                                </div>
                                
                                 <div class="form-group">
                                    <label for="input">Item Looking</label>
                                    <input name="looking" value="{{old('looking')}}" type="text" class="form-control" id="looking" placeholder="Item Looking">
                                </div>
                                
                               
                                
                                
                                <!-- Country -->
                                <div class="form-group">
                                        <label for="input">Country</label>
                                    <select id='countries' name="country_id" class="form-control">
                                        <option value="">Choose</option>
                                        @foreach($countries as $country)
                                        <option @if(old('country_id') == $country['country_id']) selected @endif value="{{$country['country_id']}}">{{$country['country_name']}}</option>
                                        @endforeach

                                    </select>

                                </div>
                                
                                <!-- State -->
                                
                                <div class="form-group">
                                        <label for="input">State</label>
                                    <select id='states' name="state_id" class="form-control">
                                        <option value="">Choose</option>
                                        

                                    </select>

                                </div>
                                
                                <!-- City -->
                                
                                <div class="form-group">
                                    <label for="input">City</label>
                                    <select id='cities' name="city_id" class="form-control">
                                        <option value="">Choose</option>
                                        

                                    </select>

                                </div>
                                
                                <!-- img, price , desc, status -->
                                
                                <div class="form-group">
                                    <label for="input">Product Image</label>
                                    <input name="product_image" value="{{old('product_img')}}" type="file" class="" id="input">
                                </div>
                                
                               <div class="form-group">
                                    <label for="input">Price</label>
                                    <input name="price" value="{{old('price')}}" type="text" class="form-control" id="price" placeholder="Price">
                                </div>
                                
                                <div class="form-group">
                                    <label for="input">postcode</label>
                                    <input name="postcode" value="{{old('postcode')}}" type="text" class="form-control" id="postcode" placeholder="Product Postcode">
                                </div>
                                
                                
                                
                                <div class="form-group">
                               
                                    <label for="input">Product Conditions</label>
                                    
                                    <select name="product_condition_id" class="form-control">
                                        <option value="">Choose</option>
                                        @foreach($product_conditions as $condition)
                                        <option @if(old('product_condition') == $condition['product_condition_id']) selected @endif value="{{$condition['product_condition_id']}}">{{$condition['product_condition_name']}}</option>
                                        @endforeach

                                    </select>
                             
                                    
                                
                                <input class="btn btn-success" type="submit" value="Post Product">
                            </form>
                        </div>
                    </div>
                    
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
