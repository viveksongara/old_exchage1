<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        
        DB::table('categories')->insert([
            'category_name' => 'T-shirt'
            
        ]);
        
        DB::table('categories')->insert([
            'category_name' => 'Skirt'
            
        ]);
        
        DB::table('categories')->insert([
            'category_name' => 'Saree'
            
        ]);
    }
}
