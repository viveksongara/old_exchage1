<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ExchangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * $table->integer('product_id');
            $table->integer('user_id');
            $table->integer('is_accepted');
            $table->date('offer_date');
            $table->time('offer_time');
         */
        DB::table('exchanges')->insert([
            'product_id' => '1',
            'product_two_id' => '4',
            'user_id'    => '2',
            'is_accepted' => '1',
            'offer_date'  => strftime('%Y-%m-%d',time()),
            'offer_time'  => strftime('%H:%M:%S',time()),
            'created_at'  => strftime('%Y-%m-%d %H:%M:%S',time())
        ]);
        
        DB::table('exchanges')->insert([
            'product_id' => '2',
            'product_two_id' => '5',
            'user_id'    => '2',
            'is_accepted' => '1',
            'offer_date'  => strftime('%Y-%m-%d',time()),
            'offer_time'  => strftime('%H:%M:%S',time()),
            'created_at'  => strftime('%Y-%m-%d %H:%M:%S',time())
        ]);
        
        DB::table('exchanges')->insert([
            'product_id' => '3',
            'product_two_id' => '6',
            'user_id'    => '2',
            'is_accepted' => '1',
            'offer_date'  => strftime('%Y-%m-%d',time()),
            'offer_time'  => strftime('%H:%M:%S',time()),
            'created_at'  => strftime('%Y-%m-%d %H:%M:%S',time())
        ]);
    }
}
