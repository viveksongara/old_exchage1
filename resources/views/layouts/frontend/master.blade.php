<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">


<!-- Mirrored from devitems.com/tf/orion-preview/orion/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 18 Sep 2016 19:06:22 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home | SwappyLand</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon
============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/frontend').'/'}}img/favicon.ico">
    
   <!-- google font CSS
============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300,800' rel='stylesheet' type='text/css'>

    <!-- style CSS
============================================ -->
    <link rel="stylesheet" href="{{asset('assets/frontend').'/'}}style.css">

    <script src="{{asset('assets/frontend').'/'}}js/vendor/modernizr-2.8.3.min.js"></script>

</head>

<body>
    <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="{{asset('assets/frontend').'/'}}http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <!-- Add your site or application content here -->

    <!-- header area start -->
    <div class="header-area">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class="single-drop">
                            <nav>
                               <ul>
                                    <li>
                                        
                                        <label><i class="fa fa-globe"></i>Language: </label><a href="#"><span>Choose language</span><i class="fa fa-angle-down"></i></a>
                                        
                                        <ul>
                                           <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      
                                        </ul>
                                    </li>
                                    <a href=""><i class="fa fa-globe"></i>Swappy</a>
                                </ul>  
                            </nav>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <div class="single-menu">
                            <nav>
                                @if(Auth::check())
                                <ul>
                                    <li><a class="lastbdr" href="{{url('user/customer-dashboard')}}">Welcome, {{Auth::user()->name}}</a></li>
                                    <li><a class="lastbdr" href="{{url('logout')}}">Logout</a></li>
                                </ul>
                                
                                @else
                                <ul>
                                    <li><a class="lastbdr" href="{{url('login')}}">Login</a></li>
                                    <li><a class="lastbdr" href="{{url('signup')}}">Register</a></li>
                                </ul>

                                @endif
                                
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="logo-area">
                            <a href="{{url('/')}}"><img src="{{asset('assets/frontend').'/'}}img/logo.png" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="block-header">
                            
                        </div>
                       @yield('search_form')
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- main-menu start -->
                        <div class="main-menu">
                            <nav>
                                <ul>
                                    <li class="bias"><a href="{{url('/')}}">Home</a>
                                        
                                    </li>
                                   
<!--                                    <li><a href="{{asset('assets/frontend').'/'}}shop.html">Hot Sale</a>
                                        <div class="mega-menu mega-menu-4 pb20 pt20">
                                            <div class="menu-carousel carousel-btn">
                                                 single-product start 
                                                <div class="col-xs-12 col-padd">
                                                    <div class="single-product">
                                                        <div class="product-label">
                                                            <div class="new"></div>
                                                        </div>
                                                        <div class="product-img">
                                                            <a href="{{asset('assets/frontend').'/'}}#">
                                                                <img class="primary-image" src="{{asset('assets/frontend').'/'}}img/product/product-1.jpg" alt="" />
                                                            </a>
                                                        </div>
                                                        <div class="product-content">
                                                            <h2 class="product-name"><a href="{{asset('assets/frontend').'/'}}#">Donec ac tempus</a></h2>
                                                            <div class="rating">
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star-o"></i></a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="new-price">$222.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 single-product end 
                                                 single-product start 
                                                <div class="col-xs-12 col-padd">
                                                    <div class="single-product">
                                                        <div class="product-label">
                                                            <div class="sale"></div>
                                                        </div>
                                                        <div class="product-img">
                                                            <a href="{{asset('assets/frontend').'/'}}#">
                                                                <img alt="" src="{{asset('assets/frontend').'/'}}img/product/product-5.jpg" class="primary-image">
                                                            </a>
                                                        </div>
                                                        <div class="product-content">
                                                            <h2 class="product-name"><a href="{{asset('assets/frontend').'/'}}#">Donec ac tempus</a></h2>
                                                            <div class="rating">
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star-o"></i></a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="new-price">$222.00</span>
                                                                <span class="old-rpice"><del>$250.00</del></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 single-product end 
                                                 single-product start 
                                                <div class="col-xs-12 col-padd">
                                                    <div class="single-product">
                                                        <div class="product-label">
                                                            <div class="new"></div>
                                                        </div>
                                                        <div class="product-img">
                                                            <a href="{{asset('assets/frontend').'/'}}#">
                                                                <img class="primary-image" src="{{asset('assets/frontend').'/'}}img/product/product-3.jpg" alt="" />
                                                            </a>
                                                        </div>
                                                        <div class="product-content">
                                                            <h2 class="product-name"><a href="{{asset('assets/frontend').'/'}}#">Donec ac tempus</a></h2>
                                                            <div class="rating">
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star-o"></i></a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="new-price">$222.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 single-product end 
                                                 single-product start 
                                                <div class="col-xs-12 col-padd">
                                                    <div class="single-product">
                                                        <div class="product-label">
                                                            <div class="sale"></div>
                                                        </div>
                                                        <div class="product-img">
                                                            <a href="{{asset('assets/frontend').'/'}}#">
                                                                <img alt="" src="{{asset('assets/frontend').'/'}}img/product/product-5.jpg" class="primary-image">
                                                            </a>
                                                        </div>
                                                        <div class="product-content">
                                                            <h2 class="product-name"><a href="{{asset('assets/frontend').'/'}}#">Donec ac tempus</a></h2>
                                                            <div class="rating">
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star-o"></i></a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="new-price">$222.00</span>
                                                                <span class="old-rpice"><del>$250.00</del></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 single-product end 
                                                 single-product start 
                                                <div class="col-xs-12 col-padd">
                                                    <div class="single-product">
                                                        <div class="product-label">
                                                            <div class="new"></div>
                                                        </div>
                                                        <div class="product-img">
                                                            <a href="{{asset('assets/frontend').'/'}}#">
                                                                <img class="primary-image" src="{{asset('assets/frontend').'/'}}img/product/product-5.jpg" alt="" />
                                                            </a>
                                                        </div>
                                                        <div class="product-content">
                                                            <h2 class="product-name"><a href="{{asset('assets/frontend').'/'}}#">Donec ac tempus</a></h2>
                                                            <div class="rating">
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star-o"></i></a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="new-price">$222.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 single-product end 
                                                 single-product start 
                                                <div class="col-xs-12 col-padd">
                                                    <div class="single-product">
                                                        <div class="product-label">
                                                            <div class="new"></div>
                                                        </div>
                                                        <div class="product-img">
                                                            <a href="{{asset('assets/frontend').'/'}}#">
                                                                <img class="primary-image" src="{{asset('assets/frontend').'/'}}img/product/product-6.jpg" alt="" />
                                                            </a>
                                                        </div>
                                                        <div class="product-content">
                                                            <h2 class="product-name"><a href="{{asset('assets/frontend').'/'}}#">Donec ac tempus</a></h2>
                                                            <div class="rating">
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star"></i></a>
                                                                <a href="{{asset('assets/frontend').'/'}}#"><i class="fa fa-star-o"></i></a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="new-price">$222.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 single-product end 
                                            </div>
                                        </div>
                                    </li>-->                      <li><a href="{{asset('user/post-product')}}">Add your Items</a></li>
                                                                   <li><a href="{{asset('shop')}}">Swaps</a></li>
                                    
                                    
                                    <li><a href="#">My items to swap</a>
                                        <ul class="sub-menu">
                                            <li><a href="{{url('user/customer-dashboard')}}">Customer Dashboard</a></li>
                                            <li><a href="{{url('user/products-applied-for-exchange')}}">Product Applied</a></li>
                                            
                                            
                                            
                                        </ul>
                                    </li>
                                    
                                    <li><a href="#">My items for exchanges</a>
                                        <ul class="sub-menu">                                            
                                            <li><a href="{{url('user/vendor-dashboard')}}">Vendor Dashboard</a></li>
                                            <li><a href="{{url('user/products')}}">My Products</a></li>
                                            <li><a href="{{url('user/exchange-requests')}}">Exchange Requests</a></li>
                                            
                                        </ul>
                                    </li>
                                    <li><a href="{{asset('assets/frontend').'/'}}about-us.html">FAQ</a></li>
                                    
                                </ul>
                            </nav>
                        </div>
                        <!-- main-menu end -->
                        <!-- mobile-menu-area start -->
                        <div class="mobile-menu-area">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <nav id="dropdown">
                                            <ul>
                                                <li><a href="{{asset('assets/frontend').'/'}}index.html">HOME</a>
                                                    <ul>
                                                        <li><a href="{{asset('assets/frontend').'/'}}index.html">Home-01</a></li>
                                                        <li><a href="{{asset('assets/frontend').'/'}}index-2.html">Home-02</a></li>
                                                        <li><a href="{{asset('assets/frontend').'/'}}index-3.html">Home-03</a></li>
                                                        <li><a href="{{asset('assets/frontend').'/'}}index-4.html">Home-04</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="{{asset('assets/frontend').'/'}}#">All Stores</a></li>
                                                <li><a href="{{asset('assets/frontend').'/'}}#">New Arrival</a></li>
                                                <li><a href="{{asset('assets/frontend').'/'}}#">Best Offer</a></li>
                                                <li><a href="{{asset('assets/frontend').'/'}}#">Hot Sale</a></li>
                                                <li><a href="{{asset('assets/frontend').'/'}}#">Fashion Trande</a></li>
                                                <li><a href="{{asset('assets/frontend').'/'}}#">PAGES</a>
                                                    <ul>
                                                        <li><a href="{{asset('assets/frontend').'/'}}cart.html">Cart</a></li>
                                                        <li><a href="{{asset('assets/frontend').'/'}}checkout.html">Check out</a></li>
                                                        <li><a href="{{asset('assets/frontend').'/'}}shop.html">Shop</a></li>
                                                        
                                                </li>
                                                <li><a href="{{asset('assets/frontend').'/'}}about-us.html">About US</a></li>
                                                <li><a href="{{asset('assets/frontend').'/'}}contact.html">Contact US</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!--mobile menu area end-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header area end -->
    
    @yield('content')
    
    
    <div class="footer-static-container">
        <div class="container">
            <div class="footer-static row pt30 pb30">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-static-title">
                        <h3>ABOUT PERSEUS</h3>
                    </div>
                    <div class="footer-static-content social-links">
                        <a href="{{url('/')}}" class="logo"><img alt="" src="{{asset('assets/frontend').'/'}}img/logo.png"></a>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas...</p>
                        <div class="web-links">
                            <ul>
                                <li><a href="{{asset('assets/frontend').'/'}}#" class="rss"><i class="fa fa-rss"></i></a></li>
                                <li><a href="{{asset('assets/frontend').'/'}}#" class="ldin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="{{asset('assets/frontend').'/'}}#" class="face"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="{{asset('assets/frontend').'/'}}#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="{{asset('assets/frontend').'/'}}#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-static-title">
                        <h3>OUR SERVICES</h3>
                    </div>
                    <div class="footer-static-content footer-service">
                        <ul>
                            <li class="first"><a href="{{url('history')}}"><i class="fa fa-angle-double-right"></i>
Our History</a></li>
                            <li><a href="{{url('returns')}}"><i class="fa fa-angle-double-right"></i>
Returns</a></li>
                            <li><a href="{{url('special')}}"><i class="fa fa-angle-double-right"></i>
Specials</a></li>
                          
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 hidden-sm col-xs-12">
                    <div class="footer-static-title">
                        <h3>MY ACCOUNT</h3>
                    </div>
                    <div class="footer-static-content footer-acc">
                        <ul>
                            <li class="first"><a href="{{url('about')}}">About us</a><span>1</span></li>
                            <li><a href="{{url('services')}}">Custom Service</a><span>2</span></li>
                            <li><a href="{{url('privacy')}}">Privacy Policy</a><span>3</span></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-static-title">
                        <h3>CONTACT US</h3>
                    </div>
                    <div class="footer-static-content footer-add">
                        <div class="address">1234 Heaven Stress, Beverly Hill OldYork- United State of Lorem</div>
                        <div class="phone">(800) 0123 4567 890
                            <br> (800) 0987 654 321</div>

                        <div class="mail"> admin@bootexparts.com
                            <br> Support@bootexparts.com</div>
                    </div>
                </div>
            </div>
            <div class="footer-static-2">
                <div class="link-category">
                   <nav>
                        <ul>
                            <li><a title="Clothing" href="{{url('terms')}}">Terms and conditon</a></li>
                            <li><a title="equipments" href="{{url('contacts')}}">Contact us</a></li>
                            
                            
                        </ul>
                   </nav>

                </div>

            </div>
        </div>
    </div>

    <footer class="ma-footer-container">
        <div class="container">
            <div class="row">
                <div class="basak-footer">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        
                            <address>Copyright &copy; 2016-All rights reserved.SwappyLand</address>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="payment">
                            <ul>
                                <li>
                                    <a href="{{asset('assets/frontend').'/'}}#"><img alt="" src="{{asset('assets/frontend').'/'}}img/switch.jpg"></a>
                                </li>
                                <li>
                                    <a href="{{asset('assets/frontend').'/'}}#"><img alt="" src="{{asset('assets/frontend').'/'}}img/american_express.jpg"></a>
                                </li>
                                <li>
                                    <a href="{{asset('assets/frontend').'/'}}#"><img alt="" src="{{asset('assets/frontend').'/'}}img/paypal.jpg"></a>
                                </li>
                                <li>
                                    <a href="{{asset('assets/frontend').'/'}}#"><img alt="" src="{{asset('assets/frontend').'/'}}img/discover.jpg"></a>
                                </li>
                                <li>
                                    <a href="{{asset('assets/frontend').'/'}}#"><img alt="" src="{{asset('assets/frontend').'/'}}img/visa.jpg"></a>
                                </li>
                                <li>
                                    <a href="{{asset('assets/frontend').'/'}}#"><img alt="" src="{{asset('assets/frontend').'/'}}img/maestro.jpg"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- JS -->

    <!-- jquery-1.11.3.min js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/vendor/jquery-1.11.3.min.js"></script>
    
    <!-- Getcity coutry -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   <script src="http://iamrohit.in/lab/js/location.js"></script>
   <!-- Get subcategories -->
   
    <script>
    $(document).ready(function(){
    
    ////////////////////////////////////////////////////////// Sub categories /////////////////////////////////////////////////////////////////
    
    $('#categories').change(function(){
        var cat_id = $(this).val();
        $.ajax({
            url: "{{url('user/get_subcategory_by_id')}}",
            type: 'GET',
            data: {category_id:cat_id},
            dataType: 'json',
            success: function (data) {
               
                var subcategories = '';
                $.each(data, function(index, element) {
                    subcategories += "<option value="+element.subcategory_id+">"+element.subcategory_name+"</option>"; 
                });
                
                $('#subcategories').empty();
                $('#subcategories').html("<option value=''>Choose</options>"+subcategories);
            }
        });
    });
    /////////////////////////////////////////////////////////// Location ////////////////////////////////////////////////////////////////
    
    $('#countries').change(function(){
        
        var country_id = $(this).val();
        
        
        $.ajax({
            url: "{{url('get-states')}}",
            type: 'GET',
            data: {country_id:country_id},
            dataType: 'json',
            success: function (data) {
                var states = '';
                
                $.each(data, function(index, element) {
                    states += "<option value='"+element.state_id+"'>"+element.state_name+"<option"; 
                });
                
                $('#states').empty();
                $('#states').html("<option value=''>Choose State</option>"+states);

            }
        });       
        

    });
    
    $('#states').change(function(){
        var state_id = $(this).val();
        
        $.ajax({
            url: "{{url('get-cities')}}",
            type: 'GET',
            data: {state_id:state_id},
            dataType: 'json',
            success: function (data) {
                
                var cities = '';
                
                $.each(data, function(index, element) {
                    cities += "<option value='"+element.city_id+"'>"+element.city_name+"</option>"; 
                });
                
                $('#cities').empty();
                $('#cities').html("<option value=''>Choose City</option>"+cities);

            }
        });
    })
    
    
    });
    </script>

    <!-- bootstrap js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/bootstrap.min.js"></script>

    <!-- Nivo slider js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}custom-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="{{asset('assets/frontend').'/'}}custom-slider/home.js" type="text/javascript"></script>

    <!-- mixit up js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/jquery.mixitup.min.js"></script>

    <!-- Fancybox up js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/fancybox/jquery.fancybox.pack.js"></script>

    <!-- Price Slider js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/jquery-price-slider.js"></script>

    <!-- owl.carousel.min js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/owl.carousel.min.js"></script>

    <!-- counterUp
============================================ -->
    <script src="cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="{{asset('assets/frontend').'/'}}js/jquery.counterup.min.js"></script>
    
    <!-- Scroll up js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/jquery.scrollUp.js"></script>

    <!-- elevator zoom js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/jquery.elevateZoom-3.0.8.min.js"></script>

    <!-- mean Menu
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/jquery.meanmenu.js"></script>

    <!-- wow js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/wow.js"></script>

    <!-- chosen.jquery.min.js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/chosen.jquery.min.js"></script>

    <!-- plugins js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/plugins.js"></script>

    <!-- main js
============================================ -->
    <script src="{{asset('assets/frontend').'/'}}js/main.js"></script>
</body>


<!-- Mirrored from devitems.com/tf/orion-preview/orion/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 18 Sep 2016 19:07:23 GMT -->
</html>