<?php

use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'state_name' => 'M.P',
            'country_id' => '1'
        ]);
        
        DB::table('states')->insert([
            'state_name' => 'U.P',
            'country_id' => '2'
        ]);
    }
}
